FROM python:3.6

LABEL maintainer "Willem Hendriks <whendrik@gmail.com>"

RUN mkdir /mlflow && chmod 777 /mlflow
WORKDIR /mlflow

RUN pip install mlflow

#
# (!) we use the none-default port of 15000 
# to avoid port conflicts
#

EXPOSE 15000

CMD mlflow server --host 0.0.0.0 -p 15000