# Mlflow

Dockerfile for [mlflow](https://mlflow.org/) - OpenShift friendly

**Note - to avoid port conflict port 15000 is chosen**

## OpenShift Build

Build with
```
oc new-app https://gitlab.com/whendrik/mlflow.git --name=mlflow
```

Delete with
```
oc delete all --selector app=mlflow
```

## mlflow basic

see [mlflow-basics](https://gitlab.com/whendrik/mlflowbasics) with the modification of the `tracking_uri`, e.g.

Get the service `IP` with

```
oc get svc
```

And use `mlflow.set_tracking_uri("172.30.102.128:15000")`

